import BlogItem from "../components/BlogItem"
import items from "../data"

const Blog = () => {
    return (
        <>
            <div className="text-[#3056D3] text-center text-lg mt-[120px]">Our Blogs</div>
            <div className="text-[#2E2E2E] text-center text-[40px] font-bold mt-[8px]">Our Recent News</div>
            <div className="text-[#637381] text-center text-[15px] mt-[15px] mb-[80px] max-w-[770px] mx-auto ">There are many variations of passages of Lorem Ipsum available
            but the majority have suffered alteration in some form.</div>
            <div className="grid grid-cols-3 max-w-all mx-auto gap-10 ">
            {
                items.map((item) => {
                    return <BlogItem data={item} />
                })
            }
            </div>            
        </>
    )
}

export default Blog