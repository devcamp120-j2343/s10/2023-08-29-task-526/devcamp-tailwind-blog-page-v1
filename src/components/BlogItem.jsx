
const BlogItem = ({data}) => {
    console.log(data);
    return (
        <div className="flex flex-col">
            <img src={data.image} className="rounded-md"/>
            <p className="bg-[#3056D3] text-white mt-[34px] mb-[25px] p-1 leading-6 text-[12px] w-[100px] ">{data.time}</p>
            <h2 className="text-[#212B36] mb-[15px] leading-8 text-[24px] font-semibold">{data.title}</h2>
            <p className="text-[#637381] leading-7 max-w text-[16px] font-normal">{data.description}</p>
        </div>
    )
}

export default BlogItem